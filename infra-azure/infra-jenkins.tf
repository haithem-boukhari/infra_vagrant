variable "vnet_address_space" {
  default = "10.0.0.0/16"
}

variable "subnet_prefix" {
    default = [
        "10.0.1.0/24",
        "10.0.2.0/24",
        "10.0.3.0/24",
        "10.0.4.0/24"
    ]

}

resource "azurerm_resource_group" "test" {
  name     = "projetfinalHaiXav"
  location = "West Europe"
}

resource "azurerm_virtual_network" "test" {
  name                = "VnetProjet"
  address_space       = ["${var.vnet_address_space}"]
  location            = "${azurerm_resource_group.test.location}"
  resource_group_name = "${azurerm_resource_group.test.name}"
}

resource "azurerm_subnet" "test1" {
  name                 = "testsubnet1"
  resource_group_name  = "${azurerm_resource_group.test.name}"
  virtual_network_name = "${azurerm_virtual_network.test.name}"
  address_prefix       = "${var.subnet_prefix[0]}"
}

resource "azurerm_subnet" "test2" {
  name                 = "testsubnet2"                                                                                
  resource_group_name  = "${azurerm_resource_group.test.name}"
  virtual_network_name = "${azurerm_virtual_network.test.name}"
  address_prefix       = "${var.subnet_prefix[1]}"
}

resource "azurerm_subnet" "test3" {
  name                 = "testsubnet3"                                                                                
  resource_group_name  = "${azurerm_resource_group.test.name}"
  virtual_network_name = "${azurerm_virtual_network.test.name}"
  address_prefix       = "${var.subnet_prefix[2]}"
}

resource "azurerm_subnet" "test4" {
  name                 = "testsubnet4"                                                                                
  resource_group_name  = "${azurerm_resource_group.test.name}"
  virtual_network_name = "${azurerm_virtual_network.test.name}"
  address_prefix       = "${var.subnet_prefix[3]}"
}

######################################################

resource "azurerm_network_security_group" "myFirstnsg" {
  name                = "nsgProjet1"
  location            = "West Europe"
  resource_group_name = "${azurerm_resource_group.test.name}"
 
 security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "inbound"
    access                     = "allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
 
 security_rule {
    name                       = "HTTP"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
 security_rule {                                                                                                       
    name                       = "port-HTTP"
    priority                   = 1003                                                                                    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"                                                                                   source_port_range          = "*"
    destination_port_range     = "8080"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_public_ip" "myFirstPubIp" {
  name                = "firstpubip"
  resource_group_name = "${azurerm_resource_group.test.name}"
  location            = "West Europe"
  allocation_method   = "Static"
}

resource "azurerm_network_interface" "myFirstNIC" {
  name                = "nameNIC1"
  location            = "West Europe"
  resource_group_name = "${azurerm_resource_group.test.name}"
  network_security_group_id = "${azurerm_network_security_group.myFirstnsg.id}"

  ip_configuration {
    name                          = "nameNICConfig1"
    subnet_id                     = "${azurerm_subnet.test1.id}"
    private_ip_address_allocation = "Static"
    private_ip_address            =  "10.0.1.4"
    public_ip_address_id          = "${azurerm_public_ip.myFirstPubIp.id}"
  }
}


resource "azurerm_virtual_machine" "myFirstVm" {
  name                  = "vm1jenkinsMaster"
  location              = "West Europe"
  resource_group_name   = "${azurerm_resource_group.test.name}"
  network_interface_ids = ["${azurerm_network_interface.myFirstNIC.id}"]
  vm_size               = "Standard_B1ms"

  storage_image_reference {
    publisher = "OpenLogic"
    offer     = "Centos"
    sku       = "7.6"
    version   = "latest"
  }
  storage_os_disk {
    name              = "myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "jenkinsMaster"
    admin_username = "stage"
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path = "/home/stage/.ssh/authorized_keys"
      key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6a/uzxbYZ/ro68ep79lRCpseThOLNifdSUFf81+f3p1WYoBi7ysVImc3tAPmO1QlQikm6Zqsdq9NAwZNWI/x1y6YrcQnttfWldBl0oo5eWNjTw+Oq//W8Y135Yo7uE4JbApOu1acqRVDaGr+jrlDynu7QLW6wseRi59SQUDf2hyvC3TfamVUWItHkOy2ihM/aJ+qlgOcud110QTyPmrWanPwUCDnQF7IRvInw5wDbNF9Oj0bG/+Ro8Dei9OKhyD8LyFQSLU09n8tCcpe2DWNcK47dgp+AB1bT/vqiyofeYWHSvTHpstzKoA6Ky3xQCue/bjDTivg8KIygU9QNysW1 vagrant@localhost.localdomain"
    }
 }
}

#################################

resource "azurerm_network_security_group" "mySecnsg" {
  name                = "nsgProjet2"
  location            = "West Europe"
  resource_group_name = "${azurerm_resource_group.test.name}"
 
 security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "inbound"
    access                     = "allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
 
 security_rule {
    name                       = "HTTP"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
 security_rule {                                                                                                       
    name                       = "port-HTTP"
    priority                   = 1003                                                                                    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"                                                                                   source_port_range          = "*"
    destination_port_range     = "8080"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_public_ip" "mySecPubIp" {
  name                = "secpubip"
  resource_group_name = "${azurerm_resource_group.test.name}"
  location            = "West Europe"
  allocation_method   = "Static"
}

resource "azurerm_network_interface" "mySecNIC" {
  name                = "nameNIC2"
  location            = "West Europe"
  resource_group_name = "${azurerm_resource_group.test.name}"
  network_security_group_id = "${azurerm_network_security_group.mySecnsg.id}"

  ip_configuration {
    name                          = "nameNICConfig2"
    subnet_id                     = "${azurerm_subnet.test2.id}"
    private_ip_address_allocation = "Static"
    private_ip_address            =  "10.0.2.6"
    public_ip_address_id          = "${azurerm_public_ip.mySecPubIp.id}"
  }
}


resource "azurerm_virtual_machine" "mySecVm" {
  name                  = "vm2jenkinsSlave"
  location              = "West Europe"
  resource_group_name   = "${azurerm_resource_group.test.name}"
  network_interface_ids = ["${azurerm_network_interface.mySecNIC.id}"]
  vm_size               = "Standard_B1ms"

  storage_image_reference {
    publisher = "OpenLogic"
    offer     = "Centos"
    sku       = "7.6"
    version   = "latest"
  }
  storage_os_disk {
    name              = "myosdisk2"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "jenkinsSlave"
    admin_username = "stage"
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path = "/home/stage/.ssh/authorized_keys"
      key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6a/uzxbYZ/ro68ep79lRCpseThOLNifdSUFf81+f3p1WYoBi7ysVImc3tAPmO1QlQikm6Zqsdq9NAwZNWI/x1y6YrcQnttfWldBl0oo5eWNjTw+Oq//W8Y135Yo7uE4JbApOu1acqRVDaGr+jrlDynu7QLW6wseRi59SQUDf2hyvC3TfamVUWItHkOy2ihM/aJ+qlgOcud110QTyPmrWanPwUCDnQF7IRvInw5wDbNF9Oj0bG/+Ro8Dei9OKhyD8LyFQSLU09n8tCcpe2DWNcK47dgp+AB1bT/vqiyofeYWHSvTHpstzKoA6Ky3xQCue/bjDTivg8KIygU9QNysW1 vagrant@localhost.localdomain"
    }
 }
}

#################################