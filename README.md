**Projet Speedy**






**Marche à suivre pour l'exécution :**

*Préparations :*
Cloner les projets et adapter chaque fichier en fonction des infrastructures de l'utilisateur (adresses des dépôts GIT, IPs publiques sur Azure, etc.) Attention : les IP doivent être changées après l'étape 4) et avant l'étape 5).


*Étapes :*

0) Sur la machine locale :
Dans le dossier contenant le Vagrantfile, créer le dossier « data ».
Dans ce dossier, il faut mettre les scripts terraform, le Jenkinsfile, les rôles Ansible



1) exécuter la commande

`> vagrant up`

dans le dossier contenant le Vagrantfile.
Cette commande installe également, dans la machine virtuelle locale :
* [git]
* [java]
* [ansible]
* [terraform]


2) se connecter en ssh à la machine vagrant par la commande

`> vagrant ssh`

3) dans les dossiers /vagrant_data/infra-azure et /vagrant_data/infra-azure-prod , créer un fichier backend.tfvars dont la structure est la suivante (les capitales devant être remplacées par les valeurs correspondant respectivement au premier et au second comptes Azure utilisés) :


`subscription_id = "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"`

`client_id       = "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"`

`client_secret   = "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"`

`tenant_id       = "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"`



4) exécuter, dans la machine virtuelle, les commandes suivantes :

`> cd /vagrant_data`

`> git clone https://gitlab.com/haithem-boukhari/infra_vagrant.git`

`> cd infra-azure`

`> terraform init`

`> terraform plan -var-file=backend.tfvars`
`> terraform apply -var-file=backend.tfvars`
`> cd ../infra-azure-prod`
`> terraform init`
`> terraform plan -var-file=backend.tfvars -var-file=main.tfvars`
`> terraform apply -var-file=backend.tfvars -var-file=main.tfvars``

5) Ceci crée une première machine virtuelle sur le cloud Azure. Il faut en relever l'IP publique, puis la reporter dans les fichiers /vagrant_data/infra-jenkinsMaster/inventory et /vagrant_data/infra-jenkinsSlave/inventory . Ensuite, exécuter les commandes suivantes :

`> ansible-playbook ../infra-jenkinsMaster/installer.yml`

`> ansible-playbook ../infra-jenkinsSlave/installer.yml`

6) utiliser un navigateur et se connecter, sur le port 8080, à l'IP relevée ci-dessus. Il faut suivre les instruction de connexion à Jenkins qui s'affichent, installer les plugins suggérés, puis installer les plugins supplémentaires suivants :
* Ansible
* Docker
* sshagent
* Azure service principal
* Gitlab
* Multi Branch source
* Multi Branch pipeline


7) dans Jenkins, créer un nœud, puis le lier avec l'installation de Jenkins qui est utilisée pour l'opération.

8) créer un Credential nommé « stage » avec la clef privée fournie par clef USB. (L'utilisateur confirmé pourra générer lui-même un trousseau de clefs ssh, puis remplacer la clef publique dans tous les fichiers où elle est utilisée.)

9) il faut ensuite entrer, dans Jenkins, un « fichier secret », pour configurer un Credential nommé « backendXP » dont le contenu est celui du fichier backend.tfvars renseigné à l'étape 3) dans le fichier /vagrant_data/infra-azure-prod/backend.tfvars .

10) créer un Credential nommé « credential_id_haithem » de type « Microsoft Azure Service Principal » contenant les identifiants de l'autre compte azure (contenu du fichier backend.tfvars renseigné au 3) dans le fichier /vagrant_data/infra-azure-prod/backend.tfvars).

11) créer un Multibranch Pipeline dans lequel il suffit de renseigner l'adresse du clone du dépôt [https://gitlab.com/haithem-boukhari/Restful-Webservice.git](https://gitlab.com/haithem-boukhari/Restful-Webservice.git) . Ne pas modifier les autres paramètres par défaut.

12) créer un webhook sur chaque branche du clone du dépôt [https://gitlab.com/haithem-boukhari/Restful-Webservice.git](https://gitlab.com/haithem-boukhari/Restful-Webservice.git) et le Multibranch Pipeline précédent.

13) connecter un navigateur à l'adresse : http:// "IP de la machine virtuelle pubipProd" :8080/customers/Tartempion
